﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;

namespace ComputerStock.Models
{
    public class ItemQuantityTableDB
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

        #region Item Plus One
        //--- Item Plus One ---//
        public int ItemPlusOne(string itemTypeId)
        {
            using (IDbConnection conn = con)
            {

                string spName = "ItemPlusOne";

                var p = new DynamicParameters();
                p.Add("itemTypeId", itemTypeId, dbType: DbType.String);
                p.Add("newItemQuantity", dbType: DbType.Int32, direction: ParameterDirection.Output);

                con.Query<int>(spName, p, commandType: CommandType.StoredProcedure);

                int newItemQuantity = p.Get<int>("newItemQuantity");

                return newItemQuantity;
            }

        }
        //--- END ---//
        #endregion

        #region Item Minus One
        //--- Item Minus One ---//
        public int ItemMinusOne(string itemTypeId)
        {
            using (IDbConnection conn = con)
            {

                string spName = "ItemMinusOne";

                var p = new DynamicParameters();
                p.Add("itemTypeId", itemTypeId, dbType: DbType.String);
                p.Add("newItemQuantity", dbType: DbType.Int32, direction: ParameterDirection.Output);

                con.Query<int>(spName, p, commandType: CommandType.StoredProcedure);

                int newItemQuantity = p.Get<int>("newItemQuantity");

                return newItemQuantity;
            }

        }
        //--- END ---//
        #endregion
    }
}