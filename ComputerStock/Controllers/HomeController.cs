﻿using ComputerStock.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ComputerStock.Controllers
{
    public class HomeController : Controller
    {
        HddTypeTableDB hddDbObj = new HddTypeTableDB();


        public ActionResult Index()
        {
            return View();
        }

        //--- User End Theme

        #region Home
        public ActionResult Home()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult Services()
        {
            return View();
        }
        public ActionResult EWaste()
        {
            return View();
        }
        #endregion

        #region Ram
        public ActionResult RamTypePartial()
        {
            return PartialView("~/Views/Partials/_RamTypePartial.cshtml");
        }
        public ActionResult RamSizeTypePartial(string ramSize)
        {
            ViewBag.RamSize = ramSize;
            return PartialView("~/Views/Partials/_RamSizeTypePartial.cshtml");
        } 
        public ActionResult RamDdrTypePartial(string ramSize,int ddrType)
        {
            ViewBag.RamSize = ramSize;
            ViewBag.DDRType = ddrType;
            return PartialView("~/Views/Partials/_RamDdrTypePartial.cshtml");
        }
        #endregion

        #region HDD
        public ActionResult HddTypePartial()
        {
            return PartialView("~/Views/Partials/_HddTypePartial.cshtml");
        }
        public ActionResult HddSizeTypePartial(decimal hddSize)
        {
            ViewBag.HddSize = hddSize;
            return PartialView("~/Views/Partials/_HddSizeTypePartial.cshtml");
        }
        public ActionResult HddSubTypePartial(decimal hddSize, string hddSubType)
        {

           List<HddTypeTable> hddTypeList = hddDbObj.GetHddByTypeNSize(hddSize, hddSubType);

            ViewBag.HddSize = hddSize;
            ViewBag.HddSubType = hddSubType;
            return PartialView("~/Views/Partials/_HddSubTypePartial.cshtml",hddTypeList);
        }

        #endregion

        #region PROCESSOR
        public ActionResult ProcessorTypePartial()
        {
            return PartialView("~/Views/Partials/_ProcessorTypePartial.cshtml");
        }
        #endregion



    }
}